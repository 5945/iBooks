package my.controller;

import java.io.IOException;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.sjcx.common.HtmlUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import my.common.BaseController;

@Controller
public class MainController extends BaseController{
	
	private static HtmlUtil htmlRegexp;
	private static String[] netUrl = {
			"http://www.x23us.com/html/0/344/"
	};
	static Document doc = null;
	/**
	 * 获取章节
	 */
	public static String getContentUrl(int num){
		doc = null;
		HashMap<Object, Object> map = new HashMap<Object, Object>();
		try {
			doc = Jsoup.connect(netUrl[0]).get();
			
			Element content = doc.getElementById("at");
			Elements links = content.getElementsByTag("a");
			int i = 0;
			for (Element link : links) {
			  String linkHref = link.attr("href");
			  String linkText = link.text();
			  i++;
			  map.put(i, netUrl[0]+linkHref);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map.get(num).toString();
	}
	
	public static String getContent(int num){
		doc = null;
		String con = null;
		try {
			doc = Jsoup.connect(getContentUrl( num )).get();
			Element content = doc.getElementById("contents");
			con = content.toString();
			con = con.replaceAll("２３Ｕｓ．ｃｏｍ", "");
			con = con.replaceAll("(看小说到顶点小说网www.x23us.com)16977小游戏每天更新好玩的小游戏，等你来发现！", "");
//			System.out.println( con );
		} catch (IOException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	@RequestMapping(value="/page/{pageNum}" , method=RequestMethod.GET)
	public String returnGetTest(ModelMap model , @PathVariable("pageNum") String content){
		
		model.addAttribute("content", getContent( Integer.parseInt(content) ));
		//toWrite(name);
		return "name";
	}
	
	public static void main(String[] args) {
		
		//z = ((x*0.025)+x)*y
		double eggs = 531;
		double count = 1000;
		int i = 1;
		while( eggs < count){
			double daysEgg = eggs*0.025;
			eggs = daysEgg + eggs;
			System.out.println("第"+ i +"天,总共:"+eggs+"   ---   每天:" + daysEgg);
			if(eggs < count)
				i++;
		}
		System.out.println("还有 "+i+" 天回本");
		/*
		int days = 50;
		for (int i = 0; i < days; i++) {
			double daysEgg = eggs*0.025;
			eggs = daysEgg + eggs;
			System.out.println("第"+ (i+1) +"天,总共:"+eggs+"   ---   每天:" + daysEgg);
		}
		*/
		
//		for(int i=0;i<10000;i++){
//			final int count = 1;
//			new Thread(){
//				
//				@Override
//				public void run() {
//					HttpClient httpClient = new HttpClient();
//					
//					while(true){
//						try{
//							PostMethod postMethod=new PostMethod("http://www.enduo168.com");
//							httpClient.executeMethod(postMethod);
//							postMethod.getResponseBodyAsString();
//							count++;
//							System.out.println("当前 = " +count);
//							Thread.sleep(1);
//							
//						}catch(Exception e){
//							System.out.println("当前 = " +count);
//							e.printStackTrace();
//						}
//					}
//				}
//			}.start();
//			
//		}
	}
}
