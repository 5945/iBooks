package my.util.monitor;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

public class CacheMgr {  
    
    // 缓存
	private static Hashtable<String, Map<String,Object>> cacheTb = new Hashtable<String, Map<String,Object>>();
	private static final String defaultCacheName = "defaultCache3944730202854873535L";
	private static final String defaultQueenCacheKey = "defaultQueenCacheKey3944730202854873535L";//in hashTable
	private static final String defaultVectorCacheName = "defaultVectorCacheName3944730202854873535L";//in map
    private CacheMgr(){
    	super();
    }
    static {
    	createCache(defaultCacheName);
    }
    
    private static void init(){
    	if(null == cacheTb){
    		cacheTb = new Hashtable<String, Map<String,Object>>();
    	}
    }
    /**
     * 
     * 方法描述:	创建1个Key为key的存储缓存
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheManager.java
     * 版本:		v1.0
     * 创建时间:	2012-10-10 下午3:57:54
     * @param cacheId	void
     */
    public static void createCache(String key){
    	init();
    	Map<String,Object> map = new HashMap<String, Object>();
    	cacheTb.put(key, map);
    }

    /**
     * 
     * 方法描述:	创建名称为key的Cache队列
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheMgr.java
     * 版本:		v1.0
     * 创建时间:	2012-10-10 下午5:41:56
     * @param key	void
     */
    public static void createQueenCache(String key){
    	init();
    	List<Object> list = new LinkedList<Object>();
    	putCache(defaultQueenCacheKey,key, list);
    }
    
    /**
     * 
     * 方法描述:	缓存cache队列
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheMgr.java
     * 版本:		v1.0
     * 创建时间:	2012-10-11 上午9:15:48
     * @param key
     * @param cache	void
     */
    @SuppressWarnings("unchecked")
	public static void putQueenCache(String key,Object cache){
    	init();
    	Map<String,Object> map = cacheTb.get(defaultQueenCacheKey);
    	Object obj = getCache(defaultQueenCacheKey, key);
    	if(null == obj || null == map){
    		createQueenCache(key);
    	}
    	List<Object> list = (LinkedList<Object>)getCache(defaultQueenCacheKey, key);
    	list.add(cache);
    }
    
    /**
     * 
     * 方法描述:根据键名，获取缓存队列	
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheMgr.java
     * 版本:		v1.0
     * 创建时间:	2012-10-11 上午9:33:47
     * @param key
     * @return	List<Object>
     */
    @SuppressWarnings("unchecked")
	public static Object getQueenCaches(String key){
    	init();
    	Map<String,Object> map = cacheTb.get(defaultQueenCacheKey);
    	if(null == map){
    		createCache(key);
    		return null;
    	}else{
    		return getCache(defaultQueenCacheKey,key);
    	}
    }
    
    /**
     * 
     * 方法描述:	根据键名，清空缓存
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheMgr.java
     * 版本:		v1.0
     * 创建时间:	2012-10-11 上午9:34:04
     * @param key	void
     */
    public static void removeQueenCache(String key){
    	init();
    	Map<String,Object> map = cacheTb.get(defaultQueenCacheKey);
    	if(null == map){
    		return ;
    	}else{
    		map.remove(key);
    	}
    }
    /**
     * 
     * 方法描述:	缓存1个Cache对象
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheManager.java
     * 版本:		v1.0
     * 创建时间:	2012-10-10 下午4:02:49
     * @param pKey
     * @param cacheId
     * @param cache	void
     */
    public static void putCache(String pKey,String cacheId,Object cache){
    	init();
    	if(!cacheTb.containsKey(pKey)){
    		createCache(pKey);
    	}
    	Map<String,Object> map = cacheTb.get(pKey);
    	if(null != map){
    		if(cache instanceof Cache){
    			map.put(cacheId, cache);    		
    		}else{
    			Cache cache0 = new Cache(String.valueOf(UUID.randomUUID()), cache, 600000, false);
    			map.put(cacheId, cache0);    		
    		}
    	}
    }
    
    /**
     * 
     * 方法描述:	缓存1个Cache对象
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheManager.java
     * 版本:		v1.0
     * 创建时间:	2012-10-10 下午3:48:39
     * @param cacheId
     * @param cache	void
     */
    public static void putCache(String cacheId,Object cache){
    	putCache(defaultCacheName, cacheId, cache);
    }
    
    /**
     * 
     * 方法描述:	
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheManager.java
     * 版本:		v1.0
     * 创建时间:	2012-10-10 下午4:00:57
     * @param pKey
     * @param cacheId
     * @return	Object
     */
    private static Object getCache(String pKey,String cacheId){
    	init();
    	Map<String,Object> map = cacheTb.get(pKey);
    	if(null == map){
    		return null;
    	}else{
    		Object obj = map.get(cacheId);
    		if(obj instanceof Cache){
    			Cache cache = (Cache)obj;
    			return cache.getValue();
    		}else 
    			return obj;
    	}
    }
    
    /**
     * 
     * 方法描述:	获取1个Cache对象
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheManager.java
     * 版本:		v1.0
     * 创建时间:	2012-10-10 下午3:49:17
     * @param cacheId
     * @return	Object
     */
    public static Object getCache(String cacheId){
    	Object obj = new Object();
    	obj = getCache(defaultCacheName, cacheId);
    	return obj;
    }
   
    /**
     * 
     * 方法描述:	移除Cache对象
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheManager.java
     * 版本:		v1.0
     * 创建时间:	2012-10-10 下午4:05:09
     * @param pKey
     * @param cacheId	void
     */
    public static void removeCache(String pKey,String cacheId){
    	init();
    	Map<String,Object> map = cacheTb.get(pKey);
    	if(null == map){
    		return ;
    	}else{
    		map.remove(cacheId);
    	}
    }

    /**
     * 
     * 方法描述:	移除Cache对象
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheManager.java
     * 版本:		v1.0
     * 创建时间:	2012-10-10 下午4:04:59
     * @param cacheId	void
     */
    public static void removeCache(String cacheId){
    	cacheTb.get(defaultCacheName).remove(cacheId);
    }
    
    /**
     * 
     * 方法描述:	遍历缓存
     * 创建者:	Administrator
     * 项目名称:	Wrasse
     * 类名:		CacheMgr.java
     * 版本:		v1.0
     * 创建时间:	2012-10-10 下午4:12:38	void
     */
    public static void lsCache(){
    	Enumeration<String> en = cacheTb.keys();
    	System.out.println("CacheLists:");
    	while(en.hasMoreElements()){
    		String key = en.nextElement();
    		Map<String,Object> map = cacheTb.get(key);
    		System.out.println("┣"+key+"  "+map.size());
    		Iterator<?> it = map.keySet().iterator();
    		while(it.hasNext()){
    			System.out.print("┃ ");
    			String mKey = (String)it.next();
    			Object obj = map.get(mKey);
    			if(obj instanceof Cache){
    				Cache cache = (Cache)obj;
    				System.out.println("┣"+mKey+" - "+ cache.getKey());
    				Object cacheVal = cache.getValue();
    				if(cacheVal instanceof List){
        				@SuppressWarnings("unchecked")
    					List<Object> list = (List<Object>)cacheVal;
        				for(Object o:list){
        					System.out.println("┃ ┃  ┣"+o.toString());
        				}
        			}else if(cacheVal instanceof Map){
        				@SuppressWarnings("unchecked")
        				Map<Object,Object> hm = (Map<Object,Object>)cacheVal;
        				for(Map.Entry<Object,Object> m:hm.entrySet()){
        					System.out.println("┃ ┃  ┣"+m.getKey()+"\t══> "+m.getValue());
        				}
        			}
    			}
    		}
    	}
    }
    
   	public static void main(String[] args) {
		putQueenCache("queen", new Cache("1"));
		putQueenCache("queen", new Cache("2"));
		putQueenCache("queen", new Cache("3"));
		putQueenCache("queen2", new Cache("24"));
		putQueenCache("queen2", new Cache("23"));
		putQueenCache("queen2", new Cache("25"));
		putQueenCache("queen3", "3");
		putQueenCache("queen3", "4");
		lsCache();

	/*	Vector ve = new Vector();
		ve.add("");
		ve.add(1);
		putCache("q1", ve);
		Vector ve0 = (Vector)getCache("q1");
		int i = ve0.size();
		lsCache();
		System.out.println(i);*/
	}
   /**
    * 
    * 方法描述:	清空Key为pKey的缓存
    * 创建者:	Administrator
    * 项目名称:	Wrasse
    * 类名:		CacheManager.java
    * 版本:		v1.0
    * 创建时间:	2012-10-10 下午4:07:19
    * @param pKey	void
    */
   public static void clear(String pKey){
	   init();
	   Map<String,Object> map = cacheTb.get(pKey);
	   if(null != map){
		   map.clear();
	   }
   }
   
   /**
    * 
    * 方法描述:	清空默认key缓存
    * 创建者:	Administrator
    * 项目名称:	Wrasse
    * 类名:		CacheManager.java
    * 版本:		v1.0
    * 创建时间:	2012-10-10 下午4:07:54	void
    */
   public static void clear(){
	   clear(defaultCacheName);
   }
 
   /**
    * 
    * 方法描述:	缓存清空关闭	
    * 创建者:	Administrator
    * 项目名称:	Wrasse
    * 类名:		CacheManager.java
    * 版本:		v1.0
    * 创建时间:	2012-10-10 下午4:08:45	void
    */
   public static void close(){
   		cacheTb.clear();
   }
}  