package my.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class WriteToFiles {
	public void WriteToFile1(String name , String fileName) throws Exception {
		File file = new File( fileName );
		FileOutputStream fos = null;
		if (!file.exists()) {
			file.createNewFile();
		}
		fos = new FileOutputStream(file);
		byte bytes[] = new byte[1024];
		bytes = name.getBytes();
		int b = name.length();
		fos.write(bytes, 0, b);
		fos.close();
	}

	public void WriteToFile2(String name , String content) throws Exception {
		FileWriter fw = new FileWriter(name, true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.newLine();
		bw.write( content );
		bw.close();
		fw.close();
	}
	
	/**
     * 写文件
     * 
     * @param path
     *            文件的路径
     * @param content
     * 写入文件的内容
     */
    public void writerText(String path, String content,String fileName) {
 
        File dirFile = new File(path);
        if (!dirFile.exists()) {
            dirFile.mkdir();
        }
        try {
            OutputStreamWriter write = new OutputStreamWriter(new FileOutputStream(path+fileName,true),"utf-8"); 
            BufferedWriter bw1 = new BufferedWriter(write);
            bw1.write(content);
            bw1.flush();
            bw1.close();
            write.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
