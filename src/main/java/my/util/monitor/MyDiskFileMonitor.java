package my.util.monitor;

public class MyDiskFileMonitor extends DiskFileMonitor{

	public MyDiskFileMonitor(String fpath) {
		super(fpath);
	}

	public void deleteAction(Object fileName) {
		System.out.println(fileName +" 元素删除");
	}

	public void addAction(Object fileName) {
		System.out.println(fileName +" 新增元素");
		
	}

	public void updateAction(Object fileName) {
		System.out.println(fileName +" 元素更新");
		
	}


	public void isLoading(Object fileName) {
		System.out.println(fileName +" 正在加载");
	}

	public void loadSuccess(Object fileName) {
		System.out.println(fileName +" 加载成功");
	}
	
	public static void main(String[] args) {
		String filePath = "F:/monitor";
		MyDiskFileMonitor mo = new MyDiskFileMonitor(filePath);
		new Thread(mo).start();
	}
}
