package org.sjcx.jsoup;

import java.io.IOException;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

public class ParsePage {
	class Constant{
		static final String DATA_URL = "http://www.x23us.com/html/52/52234/";
	}
			
			
	
	private static String ParsePageBody(){
		Response res;
		String body = null;
		try {
			res = Jsoup.connect(Constant.DATA_URL)
					.header("Accept", "*/*")
					.header("Accept-Encoding", "gzip, deflate")
					.header("Accept-Language","zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
					.header("Content-Type", "application/json;charset=UTF-8")
					.header("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0")
					.timeout(10000).ignoreContentType(true).execute();

			body = res.body();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return body;
	}
	

	
	public static void main(String[] args) {
		System.out.println(ParsePageBody());
	}
}