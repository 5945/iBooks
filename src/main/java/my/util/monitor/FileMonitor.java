package my.util.monitor;


public interface FileMonitor extends Monitor ,Runnable{

	/** 文件删除 */
	void deleteAction(Object fileName);
	/** 文件增加 */
	void addAction(Object fileName);
	/** 文件更新 */
	void updateAction(Object fileName);
	/** 正在加载 */
	void isLoading(Object fileName);
	/** 加载成功 */
	void loadSuccess(Object fileName);
}
