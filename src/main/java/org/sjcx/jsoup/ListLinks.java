package org.sjcx.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import my.common.WriteToFiles;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Example program to list links from a URL.
 */
public class ListLinks {

	private static String hrefUrl = "http://www.biqiuge.com/book/10043/";
	private static WriteToFiles wtf = new WriteToFiles();
	static URL _url = null;
	HttpURLConnection connection = null;
	Document doc = null;
	int links_size = 0;
	String ip = null;
	String port = null;
	String TxtName = null;
	public void URLData(String ips , int ports){
		ips = ip;
		port = String.valueOf(ports);
		doc = null;
		connection = null;
		try {
			URL url = new URL( hrefUrl );
			connection = (HttpURLConnection)url.openConnection();
			//默认就是Get，可以采用post，大小写都行，因为源码里都toUpperCase了。
			connection.setRequestMethod("GET");
			//是否允许缓存，默认true。
			connection.setUseCaches(Boolean.FALSE);
			//是否开启输出输入，如果是post使用true。默认是false
			//connection.setDoOutput(Boolean.TRUE);
			//connection.setDoInput(Boolean.TRUE);
			//设置请求头信息
			connection.addRequestProperty("Connection", "close");
			//设置连接主机超时（单位：毫秒）  
			connection.setConnectTimeout(8000);  
			//设置从主机读取数据超时（单位：毫秒）  
			connection.setReadTimeout(8000);    
			//设置Cookie
//			connection.addRequestProperty("Cookie","你的Cookies" );
			//开始请求
			doc = Jsoup.parse(connection.getInputStream(), "GBK", hrefUrl);

//			傲剑凌云 最新章节更新列表
			String TxtName = doc.select("h1").text();
			
			//获取父页面所有连接
			Elements linksA = doc.select("#list");
			//根据父页面获取子页面数据
			Elements links = linksA.select("a[href]");

			links_size = links.size();
			
			long startTime=System.currentTimeMillis();   //获取开始时间
			for (int i = 0; i < links_size; i++) {
				String _link = links.get(i).attr("abs:href");
				//当前index
				int index = i + 1;
				//String abs = links.attr("abs:href");
				System.out.println("------------开始下载------------");
				System.out.println("正在下载: "+ index +" -- 剩余: "+ (links_size - index));
//				long startTimeChild=System.currentTimeMillis();   //获取开始时间  
				
//				System.out.println("我是A的link = "+_link);
				
				
				getHttpResponseCode( index , _link );
//				long endTimeChild=System.currentTimeMillis(); //获取结束时间  
//			    System.out.println(" 耗时： "+(endTimeChild-startTimeChild)+"ms");  
			}
			long endTime=System.currentTimeMillis(); //获取结束时间  
		    System.out.println(TxtName+"全书下载完成 - 耗时： "+(endTime-startTime)+"ms"); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getHttpResponseCode(int index , String _link) {
		int status = 000;
		int getIndex = 0;
		try {
			_url = new URL( _link );
			status = HttpResponse.getHttpResponseCode( _url );
			System.out.println("访问状态:"+status);
			if(status == 200){
				getUrlWrite( index , _url );
			}else{
				getIndex++;
				System.out.println("开始第 "+ getIndex +" 次下载..." + _url);
				Thread.sleep(5000);
				getHttpResponseCode( index , _link );
			}
		} catch (MalformedURLException e) {
			System.out.println("---------1------------");
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return status;
    }

	public boolean getUrlWrite( int index , URL childUrl ){
		doc = null;
		connection = null;
//		System.out.println(childUrl.toString());
		try {
//			System.setProperty("http.proxyHost", ip);
//			System.setProperty("http.proxyPort", port);
			
			int status = HttpResponse.getHttpResponseCode( _url );
			if(status != 200){
				System.out.println("-------false--------");
				return false;
			}
			connection = (HttpURLConnection)_url.openConnection();
			//默认就是Get，可以采用post，大小写都行，因为源码里都toUpperCase了。
			connection.setRequestMethod("GET");
			//是否允许缓存，默认true。
			connection.setUseCaches(Boolean.FALSE);
			//设置请求头信息
			connection.addRequestProperty("Connection", "close");
			//设置连接主机超时（单位：毫秒）  
			connection.setConnectTimeout(8000);  
			//设置从主机读取数据超时（单位：毫秒）  
			connection.setReadTimeout(8000);    
			//设置Cookie
//			connection.addRequestProperty("Cookie","你的Cookies" );
			//开始请求
			doc = Jsoup.parse(connection.getInputStream(), "GBK", childUrl.toString());
			
			//设置标题
			String title = null;
			//设置内容
			Elements content = null;
			
			title = doc.select("h1").text();
			content = doc.select("#content");

			System.out.println("title = " + title);
			System.out.println("content = " + content);
			
			String _html = content.html()
					.replaceAll("<br>&nbsp;&nbsp;&nbsp;&nbsp;", "")
					.replaceAll("<br>", "")
					.replaceAll("&nbsp;&nbsp;&nbsp;&nbsp;", "");

				wtf.writerText("F:/demo/","\n"+title+"\n"+_html, TxtName+".txt");

				System.out.println("第"+ index +"章 download OK!");

			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return true;

	}

	public static void main(String[] args) throws IOException {
		new ListLinks().URLData(null,00);
	}

}