package org.sjcx.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import my.common.WriteToFiles;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Example program to list links from a URL.
 */
public class ReardBook2 {

	private static WriteToFiles wtf = new WriteToFiles();
	static URL _url = null;
	HttpURLConnection connection = null;
	Document doc = null;
	int links_size = 0;
	String TxtName = null;
	
	/**
	 * 第一步,获取地址所有的A
	 * 
	 * @param hrefUrl : 目标地址
	 */
	public void URLData(String hrefUrl){
		doc = null;
		connection = null;
		try {
			URL url = new URL( hrefUrl );
			
			int status = HttpResponse.getHttpResponseCode( url );
			
			if( status == 200 ){
				connection = (HttpURLConnection)url.openConnection();
				//默认就是Get，可以采用post，大小写都行，因为源码里都toUpperCase了。
				connection.setRequestMethod("GET");
				//是否允许缓存，默认true。
				connection.setUseCaches(Boolean.FALSE);
				//是否开启输出输入，如果是post使用true。默认是false
				//connection.setDoOutput(Boolean.TRUE);
				//connection.setDoInput(Boolean.TRUE);
				//设置请求头信息
				connection.addRequestProperty("Connection", "close");
				//设置连接主机超时（单位：毫秒）  
				connection.setConnectTimeout(8000);  
				//设置从主机读取数据超时（单位：毫秒）  
				connection.setReadTimeout(8000);    
				//开始请求
				doc = Jsoup.parse(connection.getInputStream(), "UTF-8", hrefUrl);
				
				
				//获取父页面 class="tag-list" 的所有连接
				Elements linksA = doc.select(".tag-list");
				//根据父页面获取子页面数据
				Elements links = linksA.select("a[href]");
			
				links_size = links.size();
				
				for (int j = 0; j < links_size; j++) {
					String _link = links.get(j).attr("abs:href");
					
					getHttpResponseCode( _link );
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		
	}

	/**
	 * 读取目标页面的内容
	 * @param _link
	 * @return
	 */
	public int getHttpResponseCode(String _link) {
		int status = 000;
		int getIndex = 0;
		try {
			_url = new URL( _link );
			status = HttpResponse.getHttpResponseCode( _url );
			System.out.println("访问状态:"+status);
			if(status == 200){
				getUrlWrite( _url );
			}else{
				getIndex++;
				System.out.println("开始第 "+ getIndex +" 次下载..." + _url);
				Thread.sleep(5000);
				getHttpResponseCode(_link );
			}
		} catch (MalformedURLException e) {
			System.out.println("---------status = 000----------");
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return status;
    }

	
	/**
	 * 获取子页面的内容
	 * @param childUrl
	 * @return
	 */
	public boolean getUrlWrite(URL childUrl){
		doc = null;
		connection = null;
		try {
			
			int status = HttpResponse.getHttpResponseCode( _url );
			if(status != 200){
				System.out.println("-------getUrlWrite():false--------");
				return false;
			}
			connection = (HttpURLConnection)_url.openConnection();
			//默认就是Get，可以采用post，大小写都行，因为源码里都toUpperCase了。
			connection.setRequestMethod("GET");
			//是否允许缓存，默认true。
			connection.setUseCaches(Boolean.FALSE);
			//设置请求头信息
			connection.addRequestProperty("Connection", "close");
			//设置连接主机超时（单位：毫秒）  
			connection.setConnectTimeout(30000);  
			//设置从主机读取数据超时（单位：毫秒）  
			connection.setReadTimeout(30000);    
			//设置Cookie
//			connection.addRequestProperty("Cookie","你的Cookies" );
			//开始请求
			doc = Jsoup.parse(connection.getInputStream(), "UTF-8", childUrl.toString());
			
			//设置标题
			String title = null;
			//设置内容
			Elements content = null;
			
			title = doc.select(".tag-name").text();
			
//			优 Q171115-2193<4期>可转 可使用红利 可使用现金券 可使用加息券
			int widthLen = title.indexOf(">");
			title = title.substring(0 , widthLen);
			title = title.trim().replaceAll("-", "_").replaceAll("<", "_").replaceAll(">", "_");
			
//			优Q171115_2193_4期
			System.out.println( title );
			
			content = doc.select(".info-section1");

			String _html = content.html();

			wtf.writerText("F:/demo/",_html, title+".txt");

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return true;

	}

	public static void main(String[] args) throws IOException {
		for (int i = 0; i < 1000; i++) {
			
			new ReardBook2().URLData("http://www.xiaozhu168.com/invest-3-0-0-0-0-1-"+i+".html");
		}
	}

}