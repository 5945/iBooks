package org.sjcx.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import my.common.WriteToFiles;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;


public class ReardBook {
	private static String TYPE = "UTF-8"; //文本默认编码
	private static WriteToFiles wtf = new WriteToFiles();
	static URL _url = null;
	HttpURLConnection connection = null;
	Document doc = null;
	int links_size = 0;
	int INDEX = 0; 
	String TxtName = null;


	public void URLData(String hrefURL){

		doc = urlCommon(hrefURL);

		//获取书名
		TxtName = doc.select("#info h1").text();

		//获取父页面所有连接
		Elements linksA = doc.select("#list");
		//根据父页面获取子页面数据
		Elements links = linksA.select("a[href]");

		links_size = links.size();
		System.out.println(TxtName + "," + links_size);


		long startTime=System.currentTimeMillis();   //获取开始时间
		for (int i = 0; i < links_size; i++) {
			String _link = links.get(i).attr("abs:href");
			//当前index
			INDEX = i + 1;
			//String abs = links.attr("abs:href");
			System.out.println("------------开始下载------------");
			System.out.println("已下载[-----------/"+ numberFormat(INDEX , links_size) +"] -- 剩余: "+ (links_size - INDEX));
			getHttpResponseCode( INDEX , _link );
		}
		long endTime=System.currentTimeMillis(); //获取结束时间  
		System.out.println(TxtName+"全书下载完成 - 耗时： "+(endTime-startTime)+"ms"); 

	}

	public int getHttpResponseCode(int index , String _link) {
		int status = 000;
		int getIndex = 0;
		try {
			_url = new URL( _link );
			status = HttpResponse.getHttpResponseCode( _url );
			System.out.println("访问状态:"+status);
			if(status == 200){
				getUrlWrite( index , _link );
			}else{
				getIndex++;
				System.out.println("开始第 "+ getIndex +" 次下载..." + _url);
				Thread.sleep(5000);
				getHttpResponseCode( index , _link );
			}
		} catch (MalformedURLException e) {
			System.out.println("---------1------------");
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return status;
	}

	public boolean getUrlWrite( int index , String childUrl ){
		doc = urlCommon(childUrl);
		connection = null;

		//设置标题
		String title = doc.select(".bookname h1").text();
		//设置内容
		Elements content = doc.select("#content");

		String _html = content.html()
				.replaceAll("<br>&nbsp;&nbsp;&nbsp;&nbsp;", "")
				.replaceAll("<br>", "")
				.replaceAll("&nbsp;&nbsp;&nbsp;&nbsp;", "");

		wtf.writerText("F:/demo/","\n"+title+"\n"+_html, TxtName+".txt");

		System.out.println("第"+ index +"章 download OK!");

		return true;

	}

	/**
	 * 通用的链接方式
	 * @param hrefUrl
	 * @return
	 */
	private Document urlCommon( String hrefUrl ) {

		try {
			URL url = new URL( hrefUrl );
			connection = (HttpURLConnection)url.openConnection();
			//默认就是Get，可以采用post，大小写都行，因为源码里都toUpperCase了。
			connection.setRequestMethod("GET");
			//是否允许缓存，默认true。
			connection.setUseCaches(Boolean.FALSE);
			//是否开启输出输入，如果是post使用true。默认是false
			//connection.setDoOutput(Boolean.TRUE);
			//connection.setDoInput(Boolean.TRUE);
			//设置请求头信息
			connection.addRequestProperty("Connection", "close");
			//设置连接主机超时（单位：毫秒）  
			connection.setConnectTimeout(8000);  
			//设置从主机读取数据超时（单位：毫秒）  
			connection.setReadTimeout(8000);    
			//设置Cookie
			//			connection.addRequestProperty("Cookie","你的Cookies" );
			//开始请求
			doc = Jsoup.parse(connection.getInputStream(), TYPE, hrefUrl);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return doc;
	}

	public static void main(String[] args) throws IOException {
		new ReardBook().URLData( "http://www.biquge.info/0_477/" );
	}

	/**
	 * 求百分比
	 * @param a
	 * @param b
	 * @return 百分比
	 */
	public String numberFormat(int a, int b){
		// 创建一个数值格式化对象  
		NumberFormat numberFormat = NumberFormat.getInstance();  

		// 设置精确到小数点后2位  
		numberFormat.setMaximumFractionDigits(2);  

		String result = numberFormat.format((float) a / (float) b * 100);  

		return result+ "%";
	}


}